import 'package:dj_tiktok_van/home.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

class MySplash extends StatefulWidget {
  @override
  _MySplashState createState() => _MySplashState();
}

class _MySplashState extends State<MySplash> {
  @override

  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 5,
      navigateAfterSeconds: Home(),
      title: Text(
        'DJ TIK TOK - Roi des immatures',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
          color: Color(0xFFE99600),
        ),
      ),

      image: Image.asset(
          'assets/tk.jpg'
      ),
      backgroundColor: Colors.black,
      photoSize: 100,
      loaderColor: Color(0xFFEEDA28),
    );
  }
}
