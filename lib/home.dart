import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  //performing the http request
  String url = "https://run.mocky.io/v3/be5c9957-2df5-4f51-aedc-389a9f35bc57";
  //String setup = "";
  String punchline = "";
  String type = "";
  int val;


  final _random = new Random();
  int next(int min, int max) => min + _random.nextInt(max - min);


  ///************  Future methode for getting data     *****************


  Future<String> getData(int value) async {
    Response res = await get(url, headers: {"Accept": "application/json"});
    var data = jsonDecode(res.body);
    print(data[value]['punchline']);
    setState(() {
     // setup = data[0]['setup'];
      punchline = data[value]['punchline'];
      type = data[value]['type'];
    });
    print(res.body);
  }

  @override
  void initState() {
    // TODO: implement initState
    val = next(0,30);
    getData(val);
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF212121),
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,

          children: [
            Text(
              'DJ TIK TOK',
              style: TextStyle(
                color: Color(0xFFE99600),
                fontWeight: FontWeight.w500,
                fontSize: 28,
              ),
            ),
            SizedBox(height: 5),
            Text(
              "${type}",
              style: TextStyle(
                color: Color(0xFFE99600),
                fontSize: 18.0,
              ),
            ),
            SizedBox(
              height: 80.0,
            ),

          /*  Center(child: Container(
                width: 150,
                child: Column(
                  children: <Widget>[
                    Image.asset('assets/tk.jpg', ),
                    SizedBox(height: 20),

                  ],
                ))
            ), */
            Card(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height * 0.4,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [

                      Text(
                        "${punchline}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 26.0,

                        ),
                      ),

                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),

            GestureDetector(
              onTap: () {
                      val = next(0,30);
                      getData(val);
                      },
              child: Container(
                width: MediaQuery.of(context).size.width - 150,
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 17),
                decoration: BoxDecoration(
                  color: Color(0xFFE99600),
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Text(
                  'Générer',
                  style: TextStyle(color: Colors.white),
                ) ,

              ),
            ),
          ],
        ),
      ),
    );
  }
}