import 'package:flutter/material.dart';
import 'package:dj_tiktok_van/splashscreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DJ TIK TOK VAN',
      home: MySplash(),
      debugShowCheckedModeBanner: false,
    );
  }
}

